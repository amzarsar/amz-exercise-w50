const car = {
    make : 'Honda',
    model : 'Accord',
    year : 2020
}
// menggunakan values
const keys = Object.keys(car)
console.log(keys)
//const values = Object.values(car)
//console.log(values)
// menggunakan map
const values2 = keys.map(function(key){
    return car[key]
})
console.log(values2)
const values3 = keys.map((key) => car[key]);
console.log(values3)
function someFunction (argument){
    console.log(argument)
    return typeof argument
}

const arrowFunction = (argument)=>{
    console.log(argument)
    return typeof argument
}

function kaliBiasa (angka,pengali){
    return angka * pengali
}